# parking_app

An app for logging customers parking into parking lot.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Project Setup

```flutterfire configure```
```flutter pub add firebase_core```
```flutter pub add firebase_auth```
```flutter pub add cloud_firestore```

## Dependencies

 - [Firebase](https://firebase.google.com/docs/flutter/setup?platform=ios)