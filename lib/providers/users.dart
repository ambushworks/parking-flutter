import 'package:api/api_provider.dart';
import 'package:parking_app/models/user.dart';

class Users extends APIProvider {
  @override
  String path = '/users/';

  @override
  Function fromJson = User.fromJson;
}