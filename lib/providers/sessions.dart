import 'package:api/api_provider.dart';
import 'package:parking_app/models/session.dart';

class Sessions extends APIProvider<Session> {
  @override
  String path = '/park/sessions/';

  @override
  Function fromJson = Session.fromJson;

  List<Session> open = [];
  List<Session> filtered = [];
  List<Session> closed = [];

  @override
  Future<String?> save(object) async {
    String? result = await super.save(object);

    if (result != null) {
      Session? session = findById(result);
      if (session != null) {
        if (session.ended == null) {
          open.insert(0, session);
        } else {
          open.removeWhere((session) => session.ended != null);
        }
        filtered = [...open];
      }
    }
    return result;
  }

  Future<void> getOpen() async {
    open = await fetch(url: "open");
    filtered = [...open];
  }

  Future<void> getClosed() async {
    closed = await fetch(url: "closed/1");
  }

  void filterOpen(String searchTerm) {
    final term = searchTerm.toUpperCase();

    filtered = open.where((session) {
      String? phone = session.car.customer?.phone?.number;
      String? email = session.car.customer?.email;
      String plate = session.car.plate;

      if ((email?.contains(term) ?? false) ||
          (phone?.contains(term) ?? false) ||
          plate.contains(term)) {
        return true;
      }
      return false;
    }).toList();
  }

  Future<void> close(Session closed) async {
    await update(closed, url: '/end/${closed.id}');
    open.removeWhere((session) => session.id == closed.id);
    filtered = [...open];
  }

  void resetFilter() {
    filtered = [...open];
  }
}
