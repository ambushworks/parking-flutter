import 'package:api/api_provider.dart';
import 'package:parking_app/models/car.dart';

class Cars extends APIProvider<Car> {
  @override
  String path = '/park/cars/';

  @override
  Function fromJson = Car.fromJson;

  /// Queries api unless a small amount of results remain, then starts filtering
  /// the list. If filtered list is empty, queries api.
  Future<List<Car>> searchByLicensePlate(String plateNumber) async {
    List<Car> carList = list();

    if (carList.length < 30 && carList.isNotEmpty) {
      carList =
          carList.where((car) => car.plate.startsWith(plateNumber)).toList();

      return carList;
    }
    else if (plateNumber.isEmpty){
      return carList;
    }

    // carList is outside of range, or carList is empty
    return fetch(url: "search/$plateNumber");
  }
}
