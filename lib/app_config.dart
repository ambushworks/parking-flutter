import 'dart:developer';
import 'package:flutter/services.dart';

typedef MethodResponse<T> = void Function(T value);
enum Environment { develop, production }

class AppConfig {
  static final String kChannelName = 'flavor';
  static final String kMethodName = 'getFlavor';

  Environment? flavor;
  final String? apiBaseUrl;

  AppConfig({this.flavor, this.apiBaseUrl});

  static AppConfig? _instance;

  static AppConfig? getInstance() => _instance;

  static configure(MethodResponse fn) async {
    try {
      // final flavor =
      //     await MethodChannel(kChannelName).invokeMethod<String>(kMethodName);

      // TODO fix hardcoded flavor
      const flavor = 'develop';
      log('STARTED WITH FLAVOR $flavor');
      _setupEnvironment(flavor);
      fn(true);
    } catch (e) {
      log("Failed: '${e.toString()}'");
      log('FAILED TO LOAD FLAVOR');
      fn(false);
    }
  }

  static _setupEnvironment(String? flavorName) async {
    String? baseUrl;
    Environment? flavor;

    if (flavorName == 'develop') {
      baseUrl = 'http://localhost:8080';
      flavor = Environment.develop;
    } else if (flavorName == 'production') {
      baseUrl = 'https://recruit.ambush.works';
      flavor = Environment.production;
    }

    _instance = AppConfig(flavor: flavor, apiBaseUrl: baseUrl);
  }
}
