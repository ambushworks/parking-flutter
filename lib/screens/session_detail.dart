import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:parking_app/models/session.dart';
import 'package:parking_app/models/user.dart';
import 'package:parking_app/providers/cars.dart';
import 'package:parking_app/providers/sessions.dart';
import 'package:parking_app/widgets/customer_edit.dart';
import 'package:provider/provider.dart';

class SessionDetailScreen extends StatefulWidget {
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey;
  static const routeName = '/session-detail-screen';
  const SessionDetailScreen({super.key, required this.scaffoldMessengerKey});

  @override
  State<StatefulWidget> createState() {
    return _SessionDetailScreenState();
  }
}

class _SessionDetailScreenState extends State<SessionDetailScreen> {
  var _isInit = true;
  var _isLoading = false;
  bool _errors = false;

  late Session session;
  late String duration;

  Future<void> _initSetup() async {
    if (!mounted) return;
    try {
      var arg = ModalRoute.of(context)!.settings.arguments;

      // Check if the argument is a future from the CarItem widget.
      if (arg is Future<String?>) {
        String? sessionId = await arg;
        if (sessionId != null) {
          if (!mounted) return;
          session = Provider.of<Sessions>(context, listen: false)
              .findById(sessionId) as Session;
        } else {
          // TODO handle sessionId being null
        }
      } else {
        session = ModalRoute.of(context)!.settings.arguments as Session;
      }

      duration = session.getDuration();
    } catch (e, st) {
      print('Failed to create widget --- $e \n$st');
      _errors = true;
    }
  }

  @override
  didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      _initSetup().then((_) {
        if (!_errors) {
          setState(() {
            _isLoading = false;
          });
        }
      });
    }

    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Parking Session'), actions: []),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Consumer<Sessions>(builder: (context, sessions, child) {
              return ListView(
                children: [
                  Card(
                    elevation: 2.0,
                    margin: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: double
                              .infinity, // Make the container fill the card width
                          padding: const EdgeInsets.all(16.0),
                          color: Theme.of(context)
                              .primaryColor, // Background color for the header
                          child: Row(
                            children: [
                              const Expanded(
                                child: Text(
                                  'Session Info',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              session.ended == null
                                  ? ElevatedButton(
                                      style: const ButtonStyle(
                                        elevation:
                                            MaterialStatePropertyAll(4.0),
                                      ),
                                      onPressed: () async {
                                        Cars carsProvider = Provider.of<Cars>(
                                            context,
                                            listen: false);

                                        session.ended =
                                            DateTime.now().toLocal();

                                        setState(() {
                                          _isLoading = true;
                                        });

                                        await sessions.close(session);
                                        session =
                                            sessions.findById(session.id!) ??
                                                session;
                                        carsProvider.dirty = true;

                                        if (!mounted) return;
                                        setState(() {
                                          duration = session.getDuration();
                                          _isLoading = false;
                                        });

                                        widget.scaffoldMessengerKey.currentState
                                            ?.showSnackBar(const SnackBar(
                                                content:
                                                    Text('Session Closed')));
                                      },
                                      child: const Text('End Session'),
                                    )
                                  : const SizedBox(height: 8),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              _infoRow(Icons.directions_car, session.car.plate),
                              _infoRow(
                                Icons.calendar_month,
                                DateFormat.yMMMMd()
                                    .format(session.started.toLocal()),
                              ),
                              _infoRow(Icons.access_time,
                                  'Started: ${DateFormat.jm().format(session.started.toLocal())}'),
                              session.isEnded()
                                  ? _infoRow(
                                      Icons.timer_off,
                                      'Ended: ${session.isSameDay() ? DateFormat.jm().format(session.ended!.toLocal()) : DateFormat.Md().add_jm().format(session.ended!.toLocal())}')
                                  : const SizedBox(),
                              duration != 'No Time' ?_infoRow(Icons.hourglass_empty, duration) : const SizedBox(),
                              session.fee != null
                                  ? _infoRow(Icons.attach_money,
                                      '${session.fee} pesos')
                                  : const SizedBox(),
                              session.startedBy != null
                                  ? _infoRow(Icons.manage_accounts,
                                      '${session.startedBy?.email}')
                                  : const SizedBox(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  session.car.customer == null
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 60),
                          child: ElevatedButton(
                            onPressed: () async {
                              setState(() {
                                // _isLoading = false;
                                session.car.customer = User();
                              });
                            },
                            child: const Text('Add Customer'),
                          ),
                        )
                      : CustomerEditWidget(scaffoldMessengerKey: widget.scaffoldMessengerKey, session: session),
                ],
              );
            }),
    );
  }

  Widget _infoRow(IconData icon, String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: [
          Icon(icon, color: Theme.of(context).colorScheme.tertiary),
          const SizedBox(width: 8.0),
          Text(text),
        ],
      ),
    );
  }
}
