import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      body: Center(
        child: Container(
          margin: const EdgeInsets.only(
            top: 0,
            bottom: 20,
            left: 0,
            right: 0,
          ),
          width: 300,
          child: Image.asset('assets/images/car-logo.png'),
        ),
      ),
    );
  }
}