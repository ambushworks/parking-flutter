import 'package:flutter/material.dart';
import 'package:parking_app/models/session.dart';
import 'package:parking_app/providers/sessions.dart';
import 'package:parking_app/screens/session_detail.dart';

import 'package:provider/provider.dart';

import 'package:parking_app/models/car.dart';
import 'package:parking_app/providers/cars.dart';

import 'package:parking_app/widgets/car_item.dart';
import 'package:parking_app/widgets/license_plate_text_field.dart';
import 'package:parking_app/widgets/new_car_item.dart';

class CreateSessionScreen extends StatefulWidget {
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey;
  const CreateSessionScreen({super.key, required this.scaffoldMessengerKey});
  static const routeName = '/create-session-screen';

  @override
  State<StatefulWidget> createState() {
    return _CreateSessionScreenState();
  }
}

class _CreateSessionScreenState extends State<CreateSessionScreen> {
  /// The amount of characters before the add car button shows
  final minPlateLength = 3;

  var _isInit = true;
  bool _isLoading = false;
  String licensePlate = '';
  List<Car> previousCars = [];

  Future<void> _initSetup() async {
    previousCars = await Provider.of<Cars>(context, listen: false).fetch();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });

      _initSetup().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }

    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Start Session'),
        ),
        body: Column(
          children: [
            LicensePlateTextField(
                onLicensePlateChanged: handleLicensePlateChanged),
            Consumer<Cars>(
              builder: (ctx, cars, child) {
                if (_isLoading) {
                  return const Center(child: CircularProgressIndicator());
                } else if (previousCars.isNotEmpty) {
                  return Expanded(
                      child: ListView.builder(
                    itemCount: previousCars.length,
                    itemBuilder: (ctx, i) => CarItem(scaffoldMessengerKey: widget.scaffoldMessengerKey,
                      car: previousCars[i],
                    ),
                  ));
                } else if (licensePlate.length > minPlateLength) {
                  return GestureDetector(
                    child: NewCarItem(plate: licensePlate),
                    onTap: () async {
                      setState(() {
                        _isLoading = true;
                      });
                      Car car = Car(plate: licensePlate);
                      String id = cars.add(car) as String;
                      car.id = id;

                      Session session =
                          Session(car: car, started: DateTime.now());

                      if (!mounted) return;
                      Future<String?> sessionId =
                          Provider.of<Sessions>(context, listen: true)
                              .save(session);

                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => SessionDetailScreen(scaffoldMessengerKey: widget.scaffoldMessengerKey),
                          settings: RouteSettings(arguments: sessionId),
                        ),
                      );
                    },
                  );
                } else {
                  return const Center(
                    child: Text('No Cars Entered!'),
                  );
                }
              },
            )
          ],
        ));
  }

  /// If stored recent licence plates are empty, will start querying to see if the entered
  /// plate exists.
  void handleLicensePlateChanged(String searchPlate) async {
    String plate = searchPlate.toUpperCase();
    if (previousCars.isEmpty) {}

    try {
      List<Car> list = await Provider.of<Cars>(context, listen: false)
          .searchByLicensePlate(plate);
      setState(() {
        previousCars = list;
        licensePlate = plate;
      });
    } catch (err) {
      print(err);
      previousCars = [];
    }
  }
}
