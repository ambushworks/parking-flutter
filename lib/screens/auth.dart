import 'package:api/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:api/exceptions/http_exception.dart';
import '/generated/l10n.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  static const routeName = '/auth';

  @override
  State<StatefulWidget> createState() {
    return _AuthScreenState();
  }
}

class _AuthScreenState extends State<AuthScreen> {
  final _form = GlobalKey<FormState>();

  final _authService = AuthService();

  var _isLogin = true;

  final Map<String, String?> _authData = {
    'email': '',
    'password': '',
  };
  var _isLoading = false;

  @override
  void didChangeDependencies() async {
    setState(() {
      _isLoading = true;
    });

    if (dotenv.env['DEBUG'] == 'true') {
      _authData['email'] = dotenv.env['DEV_USER'];
      _authData['password'] = dotenv.env['DEV_PW'];
    }

    setState(() {
      _isLoading = false;
    });
    super.didChangeDependencies();
  }

  void _submit() async {
    final isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }

    _form.currentState!.save();
    setState(() {
      _isLoading = true;
    });

    try {
      if (_authData.containsKey('email') && _authData.containsKey('password')) {
        _authService.authenticate(
          _authData['email']!,
          _authData['password']!,
          urlSegment: _isLogin ? 'login' : 'signup',
        );
      }
    } on HttpException catch (error) {
      if (!mounted) return;
      ScaffoldMessenger.of(context).clearSnackBars();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(error.message)));
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(
                  top: 0,
                  bottom: 20,
                  left: 0,
                  right: 0,
                ),
                width: 300,
                child: Image.asset('assets/images/car-logo.png'),
              ),
              Card(
                  margin: const EdgeInsets.all(20),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Form(
                          key: _form,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TextFormField(
                                initialValue: _authData['email'],
                                decoration: InputDecoration(
                                  labelText: S.of(context).emailAddress,
                                ),
                                keyboardType: TextInputType.emailAddress,
                                autocorrect: false,
                                textCapitalization: TextCapitalization.none,
                                validator: (value) {
                                  if (value == null ||
                                      value.trim().isEmpty ||
                                      !value.contains('@')) {
                                    return S.of(context).validEmail;
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _authData['email'] = value!;
                                },
                              ),
                              TextFormField(
                                initialValue: _authData['password'],
                                decoration: InputDecoration(
                                  labelText: S.of(context).password,
                                ),
                                obscureText: true,
                                validator: (value) {
                                  if (value == null ||
                                      value.trim().length < 6) {
                                    return S.of(context).pwShort;
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _authData['password'] = value!;
                                },
                              ),
                              const SizedBox(height: 12),
                              if (_isLoading)
                                const CircularProgressIndicator()
                              else
                                ElevatedButton(
                                    onPressed: _submit,
                                    style: ButtonStyle(
                                      elevation: MaterialStateProperty.all(4.0),
                                      // backgroundColor:
                                      // MaterialStatePropertyAll<Color>(Theme.of(context).colorScheme.secondary),
                                    ),
                                    child: Text(_isLogin
                                        ? S.of(context).login
                                        : S.of(context).signup)),
                              TextButton(
                                  onPressed: () {
                                    setState(() {
                                      _isLogin = !_isLogin;
                                    });
                                  },
                                  child: Text(_isLogin
                                      ? S.of(context).createAccount
                                      : S.of(context).haveAccount))
                            ],
                          )),
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
