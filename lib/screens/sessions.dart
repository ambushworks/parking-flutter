import 'package:api/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:parking_app/models/session.dart';
import 'package:parking_app/providers/sessions.dart';
import 'package:parking_app/screens/create_session.dart';
import 'package:parking_app/screens/session_detail.dart';
import 'package:parking_app/widgets/session_entry.dart';
import 'package:provider/provider.dart';

import '/generated/l10n.dart';

class SessionsScreen extends StatefulWidget {
  const SessionsScreen({super.key});
  static const routeName = '/sessions-screen';

  @override
  State<StatefulWidget> createState() {
    return _SessionsScreenState();
  }
}

class _SessionsScreenState extends State<SessionsScreen> {
  bool _isLoading = false;
  late final Sessions provider;

  @override
  void didChangeDependencies() async {
    setState(() {
      _isLoading = true;
    });

    provider = Provider.of<Sessions>(context, listen: false);
    await provider.getOpen();
    await provider.getClosed();

    setState(() {
      _isLoading = false;
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            dividerColor: Theme.of(context).colorScheme.primary,
            tabs: const [
              Tab(icon: Icon(Icons.local_parking_rounded)),
              Tab(icon: Icon(Icons.receipt_long_rounded)),
            ],
          ),
          title: const Text('Parking Sessions'),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed(CreateSessionScreen.routeName);
            },
            icon: Icon(
              Icons.library_add,
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
          actions: [
            IconButton(
              onPressed: () {
                final auth = AuthService();
                auth.logout();
                Navigator.popUntil(context, (route) => route.isFirst);
              },
              icon: Icon(
                Icons.logout,
                color: Theme.of(context).colorScheme.primary,
              ),
            ),
          ],
        ),
        body: _isLoading
            ? const Center(child: CircularProgressIndicator())
            : Consumer<Sessions>(builder: (ctx, sessions, child) {
                return TabBarView(
                  children: [
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16),
                          child: TextField(
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.search),
                              hintText: S.of(context).openSessionHint,
                              hintStyle: TextStyle(
                                  color: Theme.of(context).hintColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w300),
                            ),
                            // keyboardType: TextInputType.number,
                            autocorrect: false,
                            textCapitalization: TextCapitalization.characters,
                            onChanged: (searchTerm) {
                              setState(() {
                                if (searchTerm == "") {
                                  sessions.resetFilter();
                                  return;
                                }
                                sessions.filterOpen(searchTerm);
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            itemCount: sessions.filtered.length,
                            itemBuilder: (ctx, i) => SessionEntry(
                              session: sessions.filtered[i],
                              onTapCallback: navigateAndUpdate,
                            ),
                          ),
                        )
                      ],
                    ),
                    ListView.builder(
                      itemCount: sessions.closed.length,
                      itemBuilder: (ctx, i) => SessionEntry(
                          session: sessions.closed[i],
                          onTapCallback: navigateAndUpdate),
                    )
                  ],
                );
              }),
      ),
    );
  }

  void navigateAndUpdate(Session session) async {
    await Navigator.of(context)
        .pushNamed(SessionDetailScreen.routeName, arguments: session);

    setState(() {});
  }
}
