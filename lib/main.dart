import 'package:api/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logging/logging.dart';
import 'package:parking_app/providers/users.dart';
import 'generated/l10n.dart';

import 'package:parking_app/providers/sessions.dart';
import 'package:parking_app/screens/session_detail.dart';
import 'package:parking_app/screens/sessions.dart';

import 'app_config.dart';

import './theme.dart';

import 'package:parking_app/providers/cars.dart';
import 'package:parking_app/screens/create_session.dart';
import 'package:parking_app/screens/splash.dart';
import 'package:provider/provider.dart';

import 'package:parking_app/screens/auth.dart';

void main() async {
  await dotenv.load(fileName: "lib/config/.env");
  WidgetsFlutterBinding.ensureInitialized();

  Logger.root.level = Level.INFO;
  Logger.root.onRecord.listen((record) {
    print('[${record.level.name}] ${record.loggerName} -- ${record.time} -- ${record.message}');
  });

  await AppConfig.configure((result) {
    if (result == true) {
      startApp();
    } else {
      // Add widget to show app failed to start
    }
  });
}

void startApp() {
  runApp(const App());
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<StatefulWidget> createState() => _AppState();
}

class _AppState extends State<App> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
  final _auth = AuthService();

  @override
  void didChangeDependencies() async {
    await _auth.isAuth;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    CustomAppTheme appTheme = const CustomAppTheme(isDark: false);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => Users(),
        ),
        ChangeNotifierProvider(
          create: (_) => Cars(),
        ),
        ChangeNotifierProvider(
          create: (_) => Sessions(),
        )
      ],
      child: MaterialApp(
        // These delegates make sure that the localization data for the proper language is loaded
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
        onGenerateTitle: (context) => S.of(context).appName,
        theme: appTheme.themeData,
        scaffoldMessengerKey: _scaffoldMessengerKey,
        home: StreamBuilder<AuthState>(
          stream: _auth.authState,
          builder: (context, snapshot) {
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const SplashScreen();
            }

            if (snapshot.hasData && snapshot.data == AuthState.authenticated) {
              return const SessionsScreen();
            } else {
              return const AuthScreen();
            }
          },
        ),
        routes: {
          AuthScreen.routeName: (ctx) => const AuthScreen(),
          SessionsScreen.routeName: (ctx) => const SessionsScreen(),
          CreateSessionScreen.routeName: (ctx) => CreateSessionScreen(scaffoldMessengerKey: _scaffoldMessengerKey),
          SessionDetailScreen.routeName: (ctx) => SessionDetailScreen(scaffoldMessengerKey: _scaffoldMessengerKey)
        },
      ),
    );
  }
}
