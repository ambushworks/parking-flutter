import 'package:flutter/material.dart';

class CustomMaterialColors {
  static MaterialColor darkGreyishBlue =
      MaterialColor(0xFF8d99ae, getSwatch(const Color(0xFF8d99ae)));
  static MaterialColor darkDesaturatedBlue =
      MaterialColor(0xFF2d5066, getSwatch(const Color(0xFF2d5066)));
  static MaterialColor darkModerateBlue =
      MaterialColor(0xFF457b9d, getSwatch(const Color(0xFF457b9d)));
  static MaterialColor darkBlue =
      MaterialColor(0xFF1D3557, getSwatch(const Color(0xFF1D3557)));
  static MaterialColor softCyan =
      MaterialColor(0xFFa8dadc, getSwatch(const Color(0xFFa8dadc)));
  static MaterialColor lightSoftGreen =
      MaterialColor(0xFFf1faee, getSwatch(const Color(0xFFf1faee)));
  static MaterialColor brightRed =
      MaterialColor(0xFFe63946, getSwatch(const Color(0xFFe63946)));

  static MaterialColor softTeal =
      MaterialColor(0xFF66a3a3, getSwatch(const Color(0xFF66a3a3)));
  static MaterialColor burntOrange =
      MaterialColor(0xFFcc5500, getSwatch(const Color(0xFFcc5500)));
  static MaterialColor cream =
      MaterialColor(0xFFfffdd0, getSwatch(const Color(0xFFfffdd0)));
  static MaterialColor charcoal =
      MaterialColor(0xFF36454f, getSwatch(const Color(0xFF36454f)));
  static MaterialColor alabaster =
      MaterialColor(0xFFf2f0e6, getSwatch(const Color(0xFFf2f0e6)));
  static MaterialColor sunsetOrange =
      MaterialColor(0xFFffad60, getSwatch(const Color(0xFFffad60)));
}

// Function to blend two colors
Color blend(Color c1, Color c2, double amount) {
  return Color.fromARGB(
    (c1.alpha * (1 - amount) + c2.alpha * amount).round(),
    (c1.red * (1 - amount) + c2.red * amount).round(),
    (c1.green * (1 - amount) + c2.green * amount).round(),
    (c1.blue * (1 - amount) + c2.blue * amount).round(),
  );
}

// Function to generate the color shades
Map<int, Color> getSwatch(Color color) {
  return {
    50: blend(color, Colors.white, 0.9),
    100: blend(color, Colors.white, 0.8),
    200: blend(color, Colors.white, 0.6),
    300: blend(color, Colors.white, 0.4),
    400: blend(color, Colors.white, 0.2),
    500: color,
    600: blend(color, Colors.black, 0.1),
    700: blend(color, Colors.black, 0.2),
    800: blend(color, Colors.black, 0.3),
    900: blend(color, Colors.black, 0.4),
  };
}

class CustomAppTheme {
  final bool isDark;

  const CustomAppTheme({
    this.isDark = false,
  });

  ThemeData get themeData {
    final TextTheme textTheme =
        (isDark ? ThemeData.dark() : ThemeData.light()).textTheme;
    final Color textColor = textTheme.bodyLarge!.color!;

    final ColorScheme colorSchemeDark = ColorScheme(
      brightness: Brightness.dark,
      primary: CustomMaterialColors.darkDesaturatedBlue.shade100,
      // primaryVariant: CustomMaterialColors.darkDesaturatedBlue.shade800,
      secondary: CustomMaterialColors.darkGreyishBlue,
      // secondaryVariant: CustomMaterialColors.darkGreyishBlue.shade800,
      background: CustomMaterialColors.darkDesaturatedBlue,
      surface: CustomMaterialColors.darkDesaturatedBlue.shade100,
      onBackground: textColor,
      onSurface: textColor,
      onError: Colors.black,
      onPrimary: Colors.white,
      onSecondary: Colors.white,
      error: Colors.red.shade400,
    );

    final darkTheme = ThemeData.from(
      textTheme: textTheme,
      colorScheme: colorSchemeDark,
    ).copyWith(
      highlightColor: Colors.green,
      hintColor: CustomMaterialColors.darkGreyishBlue.shade50,
      // accentColor: CustomMaterialColors.darkGreyishBlue.shade50,
      // focusColor: CustomMaterialColors.darkGreyishBlue.shade50,
      // toggleableActiveColor: Colors.black
    );

    final ColorScheme colorSchemeLight = ColorScheme(
      brightness: Brightness.light,
      primary: CustomMaterialColors.burntOrange,
      secondary: CustomMaterialColors.softTeal,
      tertiary: CustomMaterialColors.darkModerateBlue.shade400,
      background: Colors.white,
      surface: Colors.white,
      onBackground: textColor,
      onSurface: CustomMaterialColors.charcoal,
      onError: textColor,
      onPrimary: CustomMaterialColors.alabaster,
      onSecondary: Colors.white,
      error: Colors.red.shade400,
    );

    final lightTheme = ThemeData.from(
      textTheme: textTheme,
      colorScheme: colorSchemeLight,
    ).copyWith(
      highlightColor: CustomMaterialColors.brightRed,
      hintColor: CustomMaterialColors.burntOrange.shade600,
    );

    if (isDark) {
      return darkTheme;
    } else {
      return lightTheme;
    }
  }
}
