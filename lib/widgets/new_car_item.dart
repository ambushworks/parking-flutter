import 'package:flutter/material.dart';

class NewCarItem extends StatelessWidget {
  final String plate;

  const NewCarItem({super.key, required this.plate});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const Icon(Icons.add),
      title: Text(plate),
      subtitle: Text('Add New Car'),
    );
  }
}