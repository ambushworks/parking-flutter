import 'package:flutter/material.dart';
import 'package:parking_app/models/car.dart';
import 'package:parking_app/models/session.dart';
import 'package:parking_app/providers/cars.dart';
import 'package:parking_app/providers/sessions.dart';
import 'package:parking_app/screens/session_detail.dart';
import 'package:provider/provider.dart';

class CarItem extends StatelessWidget {
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey;
  final Car car;

   const CarItem(
      {super.key, required this.scaffoldMessengerKey, required this.car});

  @override
  Widget build(BuildContext context) {
    String lastSession =
        car.lastSession != null ? car.lastSession.toString() : 'No Sessions';
    return GestureDetector(
      child: ListTile(
        leading: const Icon(Icons.drive_eta),
        title: Text(car.plate),
        subtitle: Text(lastSession),
      ),
      onTap: () {
        Session session = Session(car: car, started: DateTime.now());
        Future<String?> sessionId =
            Provider.of<Sessions>(context, listen: false).save(session);

        // remove from list of available cars
        Provider.of<Cars>(context, listen: false).removeObjectInList(car);

        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => SessionDetailScreen(
                scaffoldMessengerKey: scaffoldMessengerKey),
            settings: RouteSettings(arguments: sessionId),
          ),
        );
      },
    );
  }
}
