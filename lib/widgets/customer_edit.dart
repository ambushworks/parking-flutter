import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:parking_app/models/car.dart';
import 'package:parking_app/models/session.dart';
import 'package:parking_app/models/user.dart';
import 'package:parking_app/providers/cars.dart';
import 'package:parking_app/providers/users.dart';
import 'package:provider/provider.dart';

class CustomerEditWidget extends StatefulWidget {
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey;
  final Session session;
  const CustomerEditWidget(
      {super.key, required this.scaffoldMessengerKey, required this.session});

  @override
  State<StatefulWidget> createState() {
    return _CustomerEditWidgetState();
  }
}

class _CustomerEditWidgetState extends State<CustomerEditWidget> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controller = TextEditingController();

  late User _customer;
  late PhoneNumber _phone;
  bool _isValid = false;
  bool _formClean = true;
  bool _isInit = false;
  bool _phoneInputInit = false;

  final _emailFocusNode = FocusNode();
  final _phoneFocusNode = FocusNode();

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      _customer = widget.session.car.customer ?? User();

      _phone = PhoneNumber(
          isoCode: _customer.phone?.countryCode ?? 'MX',
          phoneNumber: _customer.phone?.number);
    }

    _isInit = true;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _phoneFocusNode.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      margin: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: double.infinity, // Make the container fill the card width
            padding: const EdgeInsets.all(16.0),
            color: Theme.of(context)
                .primaryColor, // Background color for the header
            child: const Text(
              'Customer Info',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  InternationalPhoneNumberInput(
                    spaceBetweenSelectorAndTextField: 0,
                    onInputChanged: (PhoneNumber phone) {
                      if (_phoneInputInit && _formClean) {
                        setState(() {
                          _formClean = false;
                        });
                      } else {
                        _phoneInputInit = true;
                      }

                      _phone = phone;
                    },
                    onInputValidated: (bool value) {
                      if (_isValid != value) {
                        setState(() {
                          _isValid = value;
                        });
                      }
                    },
                    selectorConfig: const SelectorConfig(
                      selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                    ),
                    selectorTextStyle: TextStyle(color: Colors.black),
                    ignoreBlank: true,
                    autoValidateMode: AutovalidateMode.disabled,
                    initialValue: _phone,
                    textFieldController: controller,
                    formatInput: false,
                    onSaved: (PhoneNumber phone) {
                      String? number = phone.phoneNumber;
                      String? code = phone.isoCode;
                      if (number != null && code != null) {
                        _customer.phone =
                            Phone(countryCode: code, number: number);
                      }
                    },
                  ),
                  TextFormField(
                    initialValue: _customer.email,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(labelText: 'E-mail'),
                    textInputAction: TextInputAction.next,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(
                        RegExp(r"[a-z0-9@\.!#$%&'*+/=?^_`{|}~-]"),
                      )
                    ],
                    focusNode: _emailFocusNode,
                    onChanged: (value) {
                       if (_formClean) {
                        setState(() {
                          _formClean = false;
                        });
                      } 
                    },
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(_phoneFocusNode);
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return null;
                      }
                      if (!RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(value)) {
                        return 'Please enter a valid email.';
                      }

                      return null;
                    },
                    onSaved: (value) {
                      _customer.email = value;
                    },
                  ),
                  !_formClean
                      ? TextButton(
                          onPressed: () {
                            _saveCustomerForm().then((valid) {});
                          },
                          child: const Text('Save'),
                        )
                      : const SizedBox(
                          height: 12.0,
                        ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> _saveCustomerForm() async {
    // Check global _isValid from InternationalPhoneNumberInput onInputValidated
    if (!_isValid) {
      return false;
    }

    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return false;
    }
    _formKey.currentState!.save();
    String? customerId =
        await Provider.of<Users>(context, listen: false).save(_customer);

    if (customerId != null) {
      _customer.id = customerId;
      Car car = widget.session.car;
      car.customer = _customer;

      widget.scaffoldMessengerKey.currentState?.showSnackBar(
          const SnackBar(content: Text('Saved.')));

      if (!mounted) return false;
      await Provider.of<Cars>(context, listen: false).save(car);

      return true;
    } else {
      _formKey.currentState!.reset();
      _customer = User();
      _phone = PhoneNumber(isoCode: 'MX');
      controller.clear();

      widget.scaffoldMessengerKey.currentState?.showSnackBar(
          const SnackBar(content: Text('Failed to save customer.')));
    }
    return false;
  }
}
