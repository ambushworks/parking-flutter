import 'package:flutter/material.dart';

class LicensePlateTextField extends StatelessWidget {
  // const LicensePlateTextField({super.key});

  final Function(String) onLicensePlateChanged;

  const LicensePlateTextField({super.key, required this.onLicensePlateChanged});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: TextField(
        textCapitalization: TextCapitalization.characters,
        decoration: const InputDecoration(
          labelText: 'Enter License Plate',
          border: OutlineInputBorder(),
        ),
        onChanged: onLicensePlateChanged,
      ),
    );
  }
}
