import 'package:flutter/material.dart';
import 'package:parking_app/models/session.dart';

class SessionEntry extends StatelessWidget {
  final Session session;
  final Function(Session) onTapCallback;

  const SessionEntry({required this.session, required this.onTapCallback, super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTapCallback(session),
      child: ListTile(
        leading: const CircleAvatar(child: Icon(Icons.directions_car)),
        title: Text(session.car.plate),
        subtitle: Text(session.getDuration()),
        trailing: IconButton(
          icon: const Icon(Icons.navigate_next),
          onPressed: () {},
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}
