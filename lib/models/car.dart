import 'package:api/api_model.dart';
import 'package:parking_app/models/user.dart';

class Car extends APIModel {
  String plate;
  DateTime? lastSession;
  User? customer;

  Car({id, this.lastSession, required this.plate, this.customer})
      : super(id: id);

  static Car fromJson(Map<String, dynamic> json) {
    Car car = Car(
      id: json['id'],
      plate: json['plate'],
    );

    if (json['customer'] != null) {
      car.customer = User.fromJson(json['customer']);
    }
    if (json['lastSession'] != null) {
      car.lastSession = DateTime.parse(json['lastSession']);
    }

    return car;
  }

  @override
  Map<String, dynamic> toJson() => {
        '_id': id,
        'plate': plate,
        'customer': customer?.id,
      };
}
