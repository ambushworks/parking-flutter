import 'package:api/api_model.dart';

class Phone extends APIModel {
  String countryCode;
  String number;

  Phone({required this.countryCode, required this.number});

  @override
  Map<String, dynamic> toJson() => {
        'countryCode': countryCode,
        'number': number,
      };

  static Phone fromJson(Map<String, dynamic> json) {
    Phone phone =
        Phone(countryCode: json['countryCode'], number: json['number']);
    return phone;
  }
}

class User extends APIModel {
  String? email;
  Phone? phone;

  User({id, this.email, this.phone}) : super(id: id);

  @override
  Map<String, dynamic> toJson() => {
        '_id': id,
        'email': email,
        'phone': phone?.toJson(),
      };

  static User fromJson(Map<String, dynamic> json) {
    User user = User(
      id: json['id'],
      email: json['email'],
      phone: json['phone'] == null ? null : Phone.fromJson(json['phone']),
    );
    return user;
  }
}
