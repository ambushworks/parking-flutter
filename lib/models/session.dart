import 'package:api/api_model.dart';
import 'package:parking_app/models/car.dart';
import 'package:parking_app/models/user.dart';

class Session extends APIModel {
  Car car;
  DateTime started;
  DateTime? ended;
  double? duration;
  String? fee;
  User? startedBy;

  Session({
    id,
    required this.car,
    required this.started,
    this.ended,
    this.duration,
    this.fee,
    this.startedBy,
  }) : super(id: id);

  @override
  Map<String, dynamic> toJson() => {
        '_id': id,
        'car': car.toJson(),
      };

  static Session fromJson(Map<String, dynamic> json) {
    Session session = Session(
      id: json['id'],
      car: Car.fromJson(json['car']),
      started: DateTime.parse(json['started']),
      ended: json['ended'] == null ? null : DateTime.parse(json['ended']),
      duration: json['duration'],
      fee: json['fee']?.toString(),
      startedBy: User.fromJson(json['startedBy']),
    );
    return session;
  }

  /// Returns duration if it exists, otherwise calculates duration
  /// from started time until current.
  String getDuration() {
    double hours = 0.0;

    if (duration == null) {
      final DateTime endDate = ended ?? DateTime.now();
      Duration duration = endDate.difference(started);
      hours = duration.inMinutes / Duration.minutesPerHour;
    } else {
      hours = duration!;
    }

    return formatDuration(hours);
  }

  /// Converts hours in decimal form to formatted string
  String formatDuration(double durationParam) {
    Duration durationObj = Duration(minutes: (durationParam * 60).floor());
    int minutes = durationObj.inMinutes -
        (durationParam.floor() * Duration.minutesPerHour);

    int hours = durationObj.inHours;

    String output = "";
    if (hours > 0) {
      output += "$hours Hours ";
    }
    if (minutes > 0) {
      if (hours > 0) {
        output += "and ${minutes.toString().padLeft(2, '0')} Minutes";
      } else {
        output += "${minutes.toString()} Minutes";
      }
    }
    if (output == "") {
      output = "No Time";
    }

    return output;
  }

  bool isSameDay() {
    if (isEnded()) {
      return started.month == ended!.month && started.day == ended!.day;
    }
    return false;
  }

  bool isEnded() {
    return ended != null;
  }
}
